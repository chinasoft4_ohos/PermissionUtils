/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Raphaël Bussa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package rebus.permissionutils;

/**
 * 权限列表
 *
 * @since 2021-03-20
 */
public enum PermissionEnum {
    /**
     * 加速度传感器
     */
    BODY_SENSORS(ohos.security.SystemPermission.ACCELEROMETER),
    /**
     * 陀螺仪传感器
     */
    GYROSCOPE_SENSORS(ohos.security.SystemPermission.GYROSCOPE),
    /**
     * 日历
     */
    READ_CALENDAR(ohos.security.SystemPermission.READ_CALENDAR),
    /**
     * 读取日历
     */
    WRITE_CALENDAR(ohos.security.SystemPermission.WRITE_CALENDAR),
    /**
     * 允许应用读取联系人(联系人是敏感权限，需要申请相应的权限证书)
     */
    READ_CONTACTS(ohos.security.SystemPermission.READ_CONTACTS),
    /**
     * 允许应用程序添加，删除和修改联系人(联系人是敏感权限，需要申请相应的权限证书)
     */
    WRITE_CONTACTS(ohos.security.SystemPermission.WRITE_CONTACTS),
    /**
     * 允许应用程序从设备的存储中读取文件
     */
    READ_EXTERNAL_STORAGE(ohos.security.SystemPermission.READ_USER_STORAGE),
    /**
     * 允许应用创建或删除文件，或将数据写入设备存储中的文件。
     */
    WRITE_EXTERNAL_STORAGE(ohos.security.SystemPermission.WRITE_USER_STORAGE),
    /**
     * 允许应用程序获取设备位置。
     */
    ACCESS_FINE_LOCATION(ohos.security.SystemPermission.LOCATION),
    /**
     * 允许后台应用程序获取设备位置
     */
    ACCESS_COARSE_LOCATION(ohos.security.SystemPermission.LOCATION_IN_BACKGROUND),
    /**
     * 麦克风录音
     */
    RECORD_AUDIO(ohos.security.SystemPermission.MICROPHONE),
    /**
     * 允许应用读取电话信息。(电话是敏感权限，需要申请相应的权限证书)
     */
    READ_PHONE_STATE(ohos.security.SystemPermission.GET_TELEPHONY_STATE),
    /**
     * 允许应用程序拨打常规电话和紧急电话，而无需启动拨号程序。(电话是敏感权限，需要申请相应的权限证书)
     */
    CALL_PHONE(ohos.security.SystemPermission.PLACE_CALL),
    /**
     * 允许应用读取呼叫日志(电话是敏感权限，需要申请相应的权限证书)
     */
    READ_CALL_LOG(ohos.security.SystemPermission.READ_CALL_LOG),
    /**
     * 允许应用程序添加，删除和修改呼叫日志。(电话是敏感权限，需要申请相应的权限证书)
     */
    WRITE_CALL_LOG(ohos.security.SystemPermission.WRITE_CALL_LOG),
    /**
     * 允允许应用程序在语音邮箱中添加消息(电话是敏感权限，需要申请相应的权限证书)
     */
    ADD_VOICEMAIL(ohos.security.SystemPermission.MANAGE_VOICEMAIL),
    /**
     * 允许使用相机
     */
    CAMERA(ohos.security.SystemPermission.CAMERA),
    /**
     * 发送短信(电话是敏感权限，需要申请相应的权限证书)
     */
    SEND_SMS(ohos.security.SystemPermission.SEND_MESSAGES),
    /**
     * 允许接受短信和处理(电话是敏感权限，需要申请相应的权限证书)
     */
    RECEIVE_SMS(ohos.security.SystemPermission.RECEIVE_SMS),
    /**
     * 允许应用程序接收和处理WAP消息。
     */
    RECEIVE_WAP_PUSH(ohos.security.SystemPermission.RECEIVE_WAP_MESSAGES),
    /**
     * 空
     */
    NULL("");

    private final String permission;

    PermissionEnum(String permission) {
        this.permission = permission;
    }

    /**
     * 从项目参数中拿到参数值
     *
     * @param value
     * @return 权限名
     */
    public static PermissionEnum fromManifestPermission(String value) {
        for (PermissionEnum permissionEnum : PermissionEnum.values()) {
            if (value.equalsIgnoreCase(permissionEnum.permission)) {
                return permissionEnum;
            }
        }
        return NULL;
    }

    @Override
    public String toString() {
        return permission;
    }
}

