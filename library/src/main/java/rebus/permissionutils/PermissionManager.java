/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Raphaël Bussa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package rebus.permissionutils;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.bundle.IBundleManager;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 权限管理器
 *
 * @since 2021-04-06
 */
public class PermissionManager {

    private static PermissionManager instance;

    private FullCallback fullCallback;
    private SimpleCallback simpleCallback;
    private AskAgainCallback askAgainCallback;
    private SmartCallback smartCallback;

    private boolean askAgain = false;

    private ArrayList<PermissionEnum> permissions;
    /**
     * 授予权限
     */
    private ArrayList<PermissionEnum> permissionsGranted;
    /**
     * 没有权限
     */
    private ArrayList<PermissionEnum> permissionsDenied;
    /**
     * 永远拒绝的列表
     */
    private ArrayList<PermissionEnum> permissionsDeniedForever;
    /**
     * 准备请求的列表
     */
    private ArrayList<PermissionEnum> permissionToAsk;

    private int key = PermissionConstant.KEY_PERMISSION;

    /**
     * 构建者
     *
     * @return current instance
     */
    public static PermissionManager Builder() {
        if (instance == null) {
            instance = new PermissionManager();
        }
        return instance;
    }

    /**
     * 请求权限并返回结果
     *
     * @param ability     target ability
     * @param requestCode  requestCode
     * @param permissions  permissions
     * @param grantResults grantResults
     */
    public static void handleResult(Ability ability, int requestCode, String[] permissions, int[] grantResults) {
        handleResult(ability, null, requestCode, permissions, grantResults);
    }

    /**
     * 请求权限并返回结果
     *
     * @param fragmentX    target v4 fragment
     * @param requestCode  requestCode
     * @param permissions  permissions
     * @param grantResults grantResults
     */
    public static void handleResult(Fraction fragmentX, int requestCode, String[] permissions, int[] grantResults) {
        handleResult(null, fragmentX, requestCode, permissions, grantResults);
    }

    /**
     * 请求权限并返回结果
     *
     * @param ability
     * @param fragmentX
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    private static void handleResult(final Ability ability,
                                     final Fraction fragmentX, int requestCode, String[] permissions,
                                     int[] grantResults) {
        if (instance == null) {
            return;
        }
        if (requestCode == instance.key) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == IBundleManager.PERMISSION_GRANTED) {
                    instance.permissionsGranted.add(PermissionEnum.fromManifestPermission(permissions[i]));
                } else {
                    boolean permissionsDeniedForever = false;
                    if (ability != null) {
                        permissionsDeniedForever = ability.canRequestPermission(permissions[i]);
                    } else if (fragmentX != null) {
                        permissionsDeniedForever = fragmentX.getFractionAbility().canRequestPermission(permissions[i]);
                    }
                    if (!permissionsDeniedForever) {
                        instance.permissionsDeniedForever.add(PermissionEnum.fromManifestPermission(permissions[i]));
                    }
                    instance.permissionsDenied.add(PermissionEnum.fromManifestPermission(permissions[i]));
                    instance.permissionToAsk.add(PermissionEnum.fromManifestPermission(permissions[i]));
                }
            }
            if (instance.permissionToAsk.size() != 0 && instance.askAgain) {
                instance.askAgain = false;
                if (instance.askAgainCallback != null && instance.permissionsDeniedForever.size()
                        != instance.permissionsDenied.size()) {
                    instance.askAgainCallback.showRequestPermission(new AskAgainCallback.UserResponse() {
                        @Override
                        public void result(boolean askAgain) {
                            if (askAgain) {
                                instance.ask(ability, fragmentX);
                            } else {
                                instance.showResult();
                            }
                        }
                    });
                } else {
                    instance.ask(ability, fragmentX);
                }
            } else {
                instance.showResult();
            }
        }
    }

    /**
     * permissions an array of permission that you need to ask
     *
     * @param permissions permissions
     * @return PermissionManager
     */
    public PermissionManager permissions(ArrayList<PermissionEnum> permissions) {
        this.permissions = new ArrayList<>();
        this.permissions.addAll(permissions);
        return this;
    }

    /**
     * 请求的权限
     *
     * @param permission permission you need to ask
     * @return current instance
     */
    public PermissionManager permission(PermissionEnum permission) {
        this.permissions = new ArrayList<>();
        this.permissions.add(permission);
        return this;
    }

    /**
     * 请求多个权限
     *
     * @param permissions permission you need to ask
     * @return current instance
     */
    public PermissionManager permission(PermissionEnum... permissions) {
        this.permissions = new ArrayList<>();
        Collections.addAll(this.permissions, permissions);
        return this;
    }

    /**
     * 在未授予权限时再次询问
     *
     * @param askAgain 在未授予权限时再次询问
     * @return current instance
     */
    public PermissionManager askAgain(boolean askAgain) {
        this.askAgain = askAgain;
        return this;
    }

    /**
     * 放入回调事件
     *
     * @param callback 为请求设置FullCallback
     * @return current instance
     */
    public PermissionManager callback(FullCallback callback) {
        this.simpleCallback = null;
        this.smartCallback = null;
        this.fullCallback = callback;
        return this;
    }

    /**
     * 为请求设置simpleCallback
     *
     * @param simpleCallback 为请求设置simpleCallback
     * @return current instance
     */
    public PermissionManager callback(SimpleCallback simpleCallback) {
        this.fullCallback = null;
        this.smartCallback = null;
        this.simpleCallback = simpleCallback;
        return this;
    }

    /**
     * 为请求设置smartCallback
     *
     * @param smartCallback 为请求设置askAgainCallback
     * @return current instance
     */
    public PermissionManager callback(SmartCallback smartCallback) {
        this.fullCallback = null;
        this.simpleCallback = null;
        this.smartCallback = smartCallback;
        return this;
    }

    /**
     * 为请求设置askAgainCallback
     *
     * @param askAgainCallback 为请求设置askAgainCallback
     * @return current instance
     */
    public PermissionManager askAgainCallback(AskAgainCallback askAgainCallback) {
        this.askAgainCallback = askAgainCallback;
        return this;
    }

    /**
     * 设置自定义请求代码
     *
     * @param key 设置自定义请求代码
     * @return current 单例
     */
    public PermissionManager key(int key) {
        this.key = key;
        return this;
    }

    /**
     * 请求事件
     *
     * @param ability target ability
     *                 只需启动所有权限管理器
     */
    public void ask(Ability ability) {
        ask(ability, null);
    }

    /**
     * 请求事件重载
     *
     * @param fragmentX target v4 fragment
     *                  只需启动所有权限管理器
     */
    public void ask(Fraction fragmentX) {
        ask(null, fragmentX);
    }

    /**
     * 开始请求权限
     *
     * @param ability
     * @param fragmentX
     */
    private void ask(Ability ability, Fraction fragmentX) {
        initArray();
        String[] permissionToAsk = permissionToAsk(ability, fragmentX);
        if (permissionToAsk.length == 0) {
            showResult();
        } else {
            if (ability != null) {
                ability.requestPermissionsFromUser(permissionToAsk, key);
            } else if (fragmentX != null) {
                fragmentX.getFractionAbility().requestPermissionsFromUser(permissionToAsk, key);
            }
        }
    }

    /**
     * 得到请求的权限
     *
     * @param ability Ability
     * @param fragmentX Fraction
     * @return 需要请求的权限 String[]
     */
    private String[] permissionToAsk(Ability ability, Fraction fragmentX) {
        ArrayList<String> permissionToAsk = new ArrayList<>();
        for (PermissionEnum permission : permissions) {
            boolean isGranted = false;
            if (ability != null) {
                isGranted = PermissionUtils.isGranted(ability, permission);
            } else if (fragmentX != null) {
                isGranted = PermissionUtils.isGranted(fragmentX.getFractionAbility(), permission);
            }
            if (!isGranted) {
                permissionToAsk.add(permission.toString());
            } else {
                permissionsGranted.add(permission);
            }
        }
        return permissionToAsk.toArray(new String[permissionToAsk.size()]);
    }

    /**
     * init permissions ArrayList
     */
    private void initArray() {
        this.permissionsGranted = new ArrayList<>();
        this.permissionsDenied = new ArrayList<>();
        this.permissionsDeniedForever = new ArrayList<>();
        this.permissionToAsk = new ArrayList<>();
    }

    /**
     * 检查三种回调类型之一是否不为null并传递数据
     */
    private void showResult() {
        if (simpleCallback != null) {
            simpleCallback.result(permissionToAsk.size() == 0 || permissionToAsk.size() == permissionsGranted.size());
        }
        if (fullCallback != null) {
            fullCallback.result(permissionsGranted, permissionsDenied, permissionsDeniedForever, permissions);
        }
        if (smartCallback != null) {
            smartCallback.result(permissionToAsk.size() == 0 || permissionToAsk.size() == permissionsGranted.size(),
                    !permissionsDeniedForever.isEmpty());
        }
        instance = null;
    }
}