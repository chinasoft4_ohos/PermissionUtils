/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Raphaël Bussa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package rebus.permissionutils;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

/**
 * 权限工具类
 *
 * @since 2021-04-06
 */
public class PermissionUtils {
    /**
     * 私有构造不允许new
     */
    private PermissionUtils() {
    }

    /**
     * 如果权限允许，则返回true
     *
     * @param context 上下文
     * @param permission 请求的权限
     * @return 是否可以申请权限
     */
    public static boolean isGranted(Context context, PermissionEnum permission) {
        return context.verifySelfPermission(permission.toString()) == IBundleManager.PERMISSION_GRANTED;
    }

    /**
     * 判读权限是否允许
     *
     * @param context current context
     * @param permission all permission you need to check
     * @return 如果有一项权限未授予, 返回false
     */
    public static boolean isGranted(Context context, PermissionEnum... permission) {
        for (PermissionEnum permissionEnum : permission) {
            if (!isGranted(context, permissionEnum)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 打开设置页面
     *
     * @param context current context
     */
    public static void openApplicationSettings(Ability context) {
        Intent intent = new Intent();
        intent.setAction(IntentConstants.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setUri(Uri.parse("package:" + context.getBundleName()));
        context.startAbility(intent, 1);
    }

}
