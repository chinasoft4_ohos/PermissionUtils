# PermissionUtils

## 项目介绍

- 项目名称：PermissionUtils
- 所属系列：openharmony的第三方组件适配移植
- 功能：轻松请求权限,链式调用,简介明了
- 项目移植状态：已完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 3.0.0

## 演示效果

<img src="img/ability.gif"></img>


<img src="img/fraction.gif"></img>


<img src="img/setting.gif"></img>


## 安装教程


1.在项目根目录下的build.gradle文件中，
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:PermissionUtils:0.0.2-SNAPSHOT')
   ......  
}
```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用方法

1.请求权限
在Ability中重写 onRequestPermissionsFromUserResult
```
  @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionManager.handleResult(this, requestCode, permissions, grantResults);
    }
```
2.开始请求权限
```
 PermissionManager.Builder()
                .key(num9000)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(MainAbilitySlice.this)
                .ask(getAbility());
 ```
或请求多个权限
 ```
.permission(PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.ACCESS_FINE_LOCATION,
                        PermissionEnum.READ_CALENDAR)
```
or
 ```
ArrayList<PermissionEnum> permissionEnumArrayList = new ArrayList<>();
permissionEnumArrayList.add(PermissionEnum.WRITE_EXTERNAL_STORAGE);
permissionEnumArrayList.add(PermissionEnum.ACCESS_FINE_LOCATION);
permissionEnumArrayList.add(PermissionEnum.READ_CALENDAR);

.permissions(permissionEnumArrayList)
```
如果你只需要检查权限，只需
```
PermissionEnum permissionEnum = PermissionEnum.WRITE_EXTERNAL_STORAGE;
boolean granted = PermissionUtils.isGranted(MainAbility.this, PermissionEnum.WRITE_EXTERNAL_STORAGE);

new ToastDialog(getContext()).setText("permissionEnum.toString() + " isGranted [" + granted + "]").setAlignment(LayoutAlignment.CENTER).show();
```
3.您可以使用三种不同的回调，这取决于您的需求。

FullCallback：为您提供有关您请求的权限的所有信息

SimpleCallback：返回一个布尔值，表示是否所有许可请求都被允许

SmartCallback：返回一个布尔值，表示是否所有许可请求都被允许；以及一个布尔值，表示是否永久拒绝某些许可权

4.如果用户对许可请求回答“不再询问”，则可以使用utils将用户重定向到应用程序设置
```
PermissionUtils.openApplicationSettings(getAbility());
```

5.判断权限是否已经拥有
```
 boolean isGranted = PermissionUtils.isGranted(getAbility(), PermissionEnum.WRITE_EXTERNAL_STORAGE);
```


## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


## 版本迭代
0.0.2-SNAPSHOT

## 版权和许可信息
```
       The MIT License (MIT)

Copyright (c) 2017-2019 Raphaël Bussa

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```