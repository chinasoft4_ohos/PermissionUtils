/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.window.service.WindowManager;
import rebus.permissionutils.sample.slice.FragmentAbilitySlice;

/**
 * 描述 Fraction
 *
 * @since 2021-04-07
 */
public class FragmentAbility extends FractionAbility {
    private RequestPermission requestPermission;

    @Override
    protected void onStart(Intent intent) {
        /**
         * 沉浸式状态栏
         */
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        super.onStart(intent);
        super.setMainRoute(FragmentAbilitySlice.class.getName());
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestPermission != null) {
            requestPermission.result(requestCode, permissions, grantResults);
        }
    }

    public void setRequestPermission(RequestPermission requestPermission) {
        this.requestPermission = requestPermission;
    }
}
