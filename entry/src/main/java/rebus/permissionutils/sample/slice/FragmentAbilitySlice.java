/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import rebus.permissionutils.sample.MainFraction;
import rebus.permissionutils.sample.ResourceTable;

/**
 * Fraction的容器
 *
 * @since 2021-04-07
 */
public class FragmentAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_fragment_container);
        initView();
    }

    private void initView() {
        findComponentById(ResourceTable.Id_back).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                getAbility().terminateAbility();
            }
        });
        FractionAbility fractionAbility = (FractionAbility) getAbility();
        FractionScheduler fractionScheduler = fractionAbility.getFractionManager().startFractionScheduler();
        fractionScheduler.add(ResourceTable.Id_container,new MainFraction());
        fractionScheduler.submit();
    }
}
