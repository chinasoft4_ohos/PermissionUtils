/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample.slice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import rebus.permissionutils.AskAgainCallback;
import rebus.permissionutils.FullCallback;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.PermissionUtils;
import rebus.permissionutils.SimpleCallback;
import rebus.permissionutils.SmartCallback;
import rebus.permissionutils.sample.InfoDialog;
import rebus.permissionutils.sample.ListDialog;
import rebus.permissionutils.sample.LogUtils;
import rebus.permissionutils.sample.ResourceTable;
import rebus.permissionutils.sample.SuperButton;

import java.util.ArrayList;
import java.util.List;

/**
 * 主页面
 *
 * @since 2021-04-06
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener, FullCallback {
    private CommonDialog mCommonDialog = null;
    private SuperButton askOnePermission;
    private SuperButton askThreePermission;
    private SuperButton askOnePermissionSimple;
    private SuperButton askThreePermissionSimple;
    private SuperButton askOnePermissionSmart;
    private SuperButton askThreePermissionSmart;
    private SuperButton checkPermission;
    private final int num9000 = 9000;
    private final int num800 = 800;
    private final int num700 = 700;
    private final int num600 = 600;
    private final int num2100 = 2100;
    private final int num2000 = 2000;
    private final int num2 = 2;
    private final int num200 = 200;
    private final int num10 = 10;
    private final int num8 = 8;
    private final int num3 = 3;
    private final String allp = "allPermissionsGranted [";
    private final String kh = "]";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        inintView();
    }

    private void inintView() {
        askOnePermission = (SuperButton) findComponentById(ResourceTable.Id_ask_one_permission);
        askThreePermission = (SuperButton) findComponentById(ResourceTable.Id_ask_three_permission);
        askOnePermissionSimple = (SuperButton) findComponentById(ResourceTable.Id_ask_one_permission_simple);
        askThreePermissionSimple = (SuperButton) findComponentById(ResourceTable.Id_ask_three_permission_simple);
        askOnePermissionSmart = (SuperButton) findComponentById(ResourceTable.Id_ask_one_permission_smart);
        askThreePermissionSmart = (SuperButton) findComponentById(ResourceTable.Id_ask_three_permission_smart);
        checkPermission = (SuperButton) findComponentById(ResourceTable.Id_check_permission);

        askOnePermission.setClickedListener(this);
        askThreePermission.setClickedListener(this);
        askOnePermissionSimple.setClickedListener(this);
        askThreePermissionSimple.setClickedListener(this);
        askOnePermissionSmart.setClickedListener(this);
        askThreePermissionSmart.setClickedListener(this);
        checkPermission.setClickedListener(this);
        findComponentById(ResourceTable.Id_menu).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Display display = DisplayManager.getInstance().getDefaultDisplay(getAbility()).get();
                int width = display.getAttributes().width;
                int height = display.getAttributes().height;
                mCommonDialog = new CommonDialog(getAbility());
                Component component1 = LayoutScatter.getInstance(getContext())
                        .parse(ResourceTable.Layout_layout_x, null, true);
                initComponent(component1, mCommonDialog);
                mCommonDialog.setSize(width / LogUtils.DEBUG, height / LogUtils.WU);
                mCommonDialog.setContentCustomComponent(component1);
                mCommonDialog.setAutoClosable(true);
                mCommonDialog.setOffset(0, LogUtils.MENULATION);
                mCommonDialog.setAlignment(LayoutAlignment.RIGHT);
                mCommonDialog.show();
            }
        });
    }

    private void initComponent(Component component, final CommonDialog dialog) {
        Text setting = (Text) component.findComponentById(ResourceTable.Id_setting);
        Text fragment = (Text) component.findComponentById(ResourceTable.Id_fragment);
        Text info = (Text) component.findComponentById(ResourceTable.Id_info);
        setting.setClickedListener(new Component.ClickedListener() {
            /**
             * 设置页面 todo 跳转系统页面
             *
             * @param component Component
             */
            @Override
            public void onClick(Component component) {
                PermissionUtils.openApplicationSettings(getAbility());
                dialog.remove();
            }
        });
        /**
         * Fraction 页面
         *
         * @param component Component
         */
        fragment.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operationCommonComponts = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("rebus.permissionutils.sample")
                        .withAbilityName("rebus.permissionutils.sample.FragmentAbility")
                        .build();
                intent.setOperation(operationCommonComponts);
                getAbility().startAbility(intent);
                dialog.remove();
            }
        });
        info.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dialog.remove();
                InfoDialog infoDialog = new InfoDialog(getAbility(), "hahahhaha");
                infoDialog.show();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            /**
             * 请求一个权限
             */
            case ResourceTable.Id_ask_one_permission:
                askonepermission();
                break;
            case ResourceTable.Id_ask_three_permission:
                askThreePermission2();
                break;
            case ResourceTable.Id_ask_one_permission_simple:
                askOnePermissionSimple2();
                break;
            case ResourceTable.Id_ask_three_permission_simple:
                askThreePermissionSimple2();
                break;
            case ResourceTable.Id_ask_one_permission_smart:
                askOnePermissionSmart2();
                break;
            case ResourceTable.Id_ask_three_permission_smart:
                askThreePermissionSmart2();
                break;
            case ResourceTable.Id_check_permission:
                checkPermission2();

                break;
            default:
                break;
        }
    }

    private void checkPermission2() {
        PermissionEnum permissionEnum = PermissionEnum.WRITE_EXTERNAL_STORAGE;
        boolean isGranted = PermissionUtils.isGranted(getAbility(), PermissionEnum.WRITE_EXTERNAL_STORAGE);
        showToast(permissionEnum.toString() + " isGranted [" + isGranted + kh, getAbility());
    }

    private void askThreePermissionSmart2() {
        PermissionManager.Builder()
                .key(num2100)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.ACCESS_FINE_LOCATION,
                        PermissionEnum.READ_CALENDAR)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean isAllPermissionsGranted, boolean isSomePermissionsDeniedForever) {
                        showToast(PermissionEnum.WRITE_EXTERNAL_STORAGE.toString()
                                +
                                ", "
                                + PermissionEnum.ACCESS_FINE_LOCATION.toString()
                                + ", " + PermissionEnum.READ_CALENDAR.toString()
                                + allp + isAllPermissionsGranted
                                + "] somePermissionsDeniedForever ["
                                + isSomePermissionsDeniedForever + kh, getAbility());
                    }
                })
                .ask(getAbility());
    }

    private void askOnePermissionSmart2() {
        PermissionManager.Builder()
                .key(num2000)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean isAllPermissionsGranted, boolean isSomePermissionsDeniedForever) {
                        showToast(PermissionEnum.WRITE_EXTERNAL_STORAGE.toString()
                                + allp + isAllPermissionsGranted + "] somePermissionsDeniedForever ["
                                + isSomePermissionsDeniedForever + kh, getAbility());
                    }
                })
                .ask(getAbility());
    }

    private void askThreePermissionSimple2() {
        PermissionManager.Builder()
                .key(num600)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.ACCESS_FINE_LOCATION,
                        PermissionEnum.READ_CALENDAR)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(new SimpleCallback() {
                    @Override
                    public void result(boolean isAllPermissionsGranted) {
                        showToast(PermissionEnum.WRITE_EXTERNAL_STORAGE.toString() + ", "
                                + PermissionEnum.ACCESS_FINE_LOCATION.toString()
                                + ", " + PermissionEnum.READ_CALENDAR.toString()
                                + allp + isAllPermissionsGranted + kh, getAbility());
                    }
                })
                .ask(getAbility());
    }

    private void askOnePermissionSimple2() {
        PermissionManager.Builder()
                .key(num700)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(new SimpleCallback() {
                    @Override
                    public void result(boolean isAllPermissionsGranted) {
                        showToast(PermissionEnum.WRITE_EXTERNAL_STORAGE.toString() + allp
                                + isAllPermissionsGranted + kh, getAbility());
                    }
                })
                .ask(getAbility());
    }

    private void askThreePermission2() {
        PermissionManager.Builder()
                .key(num800)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.ACCESS_FINE_LOCATION,
                        PermissionEnum.READ_CALENDAR)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(MainAbilitySlice.this)
                .ask(getAbility());
    }

    private void askonepermission() {
        PermissionManager.Builder()
                .key(num9000)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        LogUtils.log(LogUtils.INFO, "test", "跑到这里了");
                        showDialog(response);
                    }
                })
                .callback(MainAbilitySlice.this)
                .ask(getAbility());
    }

    /**
     * 显示Toast
     *
     * @param msg
     * @param ability
     */
    private void showToast(String msg, Ability ability) {
        DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(ability)
                .parse(ResourceTable.Layout_layout_toast_and_image, null, false);
        Text text = (Text) layout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText(msg);
        Display display = DisplayManager.getInstance().getDefaultDisplay(getAbility()).get();
        int height = display.getAttributes().height;
        int width = display.getAttributes().width;
        new ToastDialog(ability)
                .setComponent(layout)
                .setSize(width / num10 * num8, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.CENTER)
                .setOffset(0, height / num2 - num200)
                .show();
    }

    /**
     * 显示个dialog 权限需要提示
     * 这个应用程式确实需要使用这个权限，您要授权吗？
     *
     * @param response
     */
    private void showDialog(final AskAgainCallback.UserResponse response) {
        CommonDialog commonDialog = new CommonDialog(getAbility());
        Component component1 = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_layout_dialog, null, true);
        inintDialogCommonDialog(component1, commonDialog, response);
        commonDialog.setContentCustomComponent(component1);
        Display display = DisplayManager.getInstance().getDefaultDisplay(getAbility()).get();
        int width = display.getAttributes().width;
        int height = display.getAttributes().height;
        float height2 = (float) ((height / num10) * num3);
        commonDialog.setSize(width / num10 * num8, (int) height2);
        commonDialog.setMovable(false);
        commonDialog.show();
    }

    private void inintDialogCommonDialog(Component component, CommonDialog commonDialog,
                                         AskAgainCallback.UserResponse response) {
        component.findComponentById(ResourceTable.Id_no).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                response.result(false);
                if (commonDialog != null) {
                    commonDialog.hide();
                    commonDialog.remove();
                }
            }
        });
        component.findComponentById(ResourceTable.Id_ok).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                response.result(true);
                if (commonDialog != null) {
                    commonDialog.hide();
                    commonDialog.remove();
                }
            }
        });
    }

    @Override
    public void result(ArrayList<PermissionEnum> permissionsGranted,
                       ArrayList<PermissionEnum> permissionsDenied, ArrayList<PermissionEnum> permissionsDeniedForever,
                       ArrayList<PermissionEnum> permissionsAsked) {
        List<String> msg = new ArrayList<>();
        for (PermissionEnum permissionEnum : permissionsGranted) {
            msg.add(permissionEnum.toString() + " [Granted]");
        }
        for (PermissionEnum permissionEnum : permissionsDenied) {
            msg.add(permissionEnum.toString() + " [Denied]");
        }
        for (PermissionEnum permissionEnum : permissionsDeniedForever) {
            msg.add(permissionEnum.toString() + " [DeniedForever]");
        }
        for (PermissionEnum permissionEnum : permissionsAsked) {
            msg.add(permissionEnum.toString() + " [Asked]");
        }
        ListDialog listDialog = new ListDialog(getAbility(), msg);
        listDialog.show();
    }
}
