/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.util.List;

/**
 * provider
 *
 * @since 2021-03-20
 */
public class ListProvider extends BaseItemProvider {
    private List<String> data;
    private Ability mainAbilitySlice;

    /**
     * 构造
     *
     * @param mainAbilitySlice
     * @param data
     */
    public ListProvider(Ability mainAbilitySlice, List<String> data) {
        this.data = data;
        this.mainAbilitySlice = mainAbilitySlice;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        if (component == null) {
            cpt = LayoutScatter.getInstance(mainAbilitySlice).parse(ResourceTable.Layout_item_sample, null, false);
        } else {
            cpt = component;
        }
        String sampleItem = data.get(position);
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_item_index);
        text.setText(sampleItem);
        return cpt;
    }
}
