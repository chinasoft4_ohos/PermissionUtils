/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;

import java.util.List;

/**
 * 描述
 *
 * @since 2021-04-08
 */
public class ListDialog {
    private static final int TEN = 10;
    private static final int NUM_50 = 50;
    private static final int NUM_6 = 6;
    private static final int NUM_12 = 12;
    private static final int NUM_9 = 9;
    private static final int NUM_86 = 86;
    private List<String> items;
    private CommonDialog mCommonDialog = null;
    private Ability context;

    /**
     * 构造
     *
     * @param context
     */
    public ListDialog(Ability context) {
        this.context = context;
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param listData 数据
     */
    public ListDialog(Ability context, List<String> listData) {
        this.context = context;
        this.items = listData;
    }

    /**
     * 显示dialog
     */
    public void show() {
        if (mCommonDialog == null) {
            mCommonDialog = new CommonDialog(context);
            Component component1 = LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_layout_list_dialog, null, true);
            initComponent(component1, mCommonDialog);
        }
    }

    private void initComponent(Component component1, CommonDialog dialog) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        int width = display.getAttributes().width;
        ListContainer listContainer = (ListContainer) component1.findComponentById(ResourceTable.Id_list_dialog);
        int simleHeight = AttrHelper.vp2px(NUM_50,context);
        int maxwidth = (width / TEN) * NUM_9;
        int maxheight = simleHeight * NUM_6;
        if (items.size() < NUM_12) {
            int size = items.size();
            int heightt = simleHeight * size;
            mCommonDialog.setSize(maxwidth,heightt + NUM_86);
        } else {
            mCommonDialog.setSize(maxwidth, maxheight + TEN + NUM_86);
        }
        ListProvider listProvider = new ListProvider(context, items);
        listContainer.setItemProvider(listProvider);
        mCommonDialog.setContentCustomComponent(component1);
        mCommonDialog.siteRemovable(true);
        mCommonDialog.setAutoClosable(true);
        mCommonDialog.setAlignment(LayoutAlignment.CENTER);
        mCommonDialog.show();
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                if (dialog != null) {
                    dialog.hide();
                    dialog.remove();
                }
            }
        });
    }

    /**
     * 消除dialog
     */
    public void cancel() {
        if (mCommonDialog != null) {
            mCommonDialog.hide();
            mCommonDialog.remove();
        }
    }
}
