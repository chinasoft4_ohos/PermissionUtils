/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * description 日志类
 *
 * @since 2021-03-20
 */
public class LogUtils {
    /**
     * 持续时间
     */
    public static final int DURATION = 2000;

    /**
     * info
     */
    public static final int INFO = 0;
    /**
     * error
     */
    public static final int ERROR = 1;
    /**
     * debug
     */
    public static final int DEBUG = 2;
    /**
     * warning
     */
    public static final int WARN = 3;
    /**
     * 返回码
     */
    public static final int REQUEST_CODE = 300;
    /**
     * System
     */
    public static final int SYETEM = 6;
    /**
     * 3
     */
    public static final int SAN = 3;
    /**
     * 5
     */
    public static final int WU = 5;
    /**
     * 2
     */
    public static final int TWO = 2;
    /**
     * 350
     */
    public static final int SANBAIWU = 350;
    /**
     * 120
     */
    public static final int YIBAIER = 120;
    /**
     * 40
     */
    public static final int SISHI = 40;
    /**
     * 400
     */
    public static final int SIBAI = 400;
    /**
     * 10
     */
    public static final int SHI = 10;
    /**
     * MENULATION
     */
    public static final int MENULATION = -920;
    /**
     * LOGTAG
     */
    public static final int LOGTAG = 0xD001100;
    /**
     * 持续时间
     */
    public static final String TAG = "wjtt";
    /**
     * ABILITY_MOTION
     */
    public static final String ABILITY_MOTION = "ohos.permission.ABILITY_MOTION";
    /**
     * GYROSCOPE
     */
    public static final String GYROSCOPE = "ohos.permission.GYROSCOPE";

    /**
     * 构造
     *
     */
    private LogUtils() {
    }

    /**
     * Log
     *
     * @param logType LogUtils.INFO || LogUtils.ERROR || LogUtils.DEBUG|| LogUtils.WARN
     * @param tag 日志标识 根据喜好，自定义
     * @param message 需要打印的日志信息
     */
    public static void log(int logType, String tag, String message) {
        HiLogLabel lable = new HiLogLabel(logType, LOGTAG, tag);
        switch (logType) {
            case INFO:
                HiLog.info(lable, message);
                break;
            case ERROR:
                HiLog.error(lable, message);
                break;
            case DEBUG:
                HiLog.debug(lable, message);
                break;
            case WARN:
                HiLog.warn(lable, message);
                break;
            default:
                break;
        }
    }
}
