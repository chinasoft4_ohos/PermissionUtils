/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import rebus.permissionutils.AskAgainCallback;
import rebus.permissionutils.FullCallback;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.PermissionUtils;
import rebus.permissionutils.SimpleCallback;
import rebus.permissionutils.SmartCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述
 *
 * @since 2021-04-07
 */
public class MainFraction extends Fraction implements Component.ClickedListener, FullCallback {
    private static final int NUM_9001 = 9001;
    private static final int NUM_801 = 801;
    private static final int NUM_701 = 701;
    private static final int NUM_601 = 601;
    private static final int NUM_2101 = 2101;
    private static final int NUM_2001 = 2001;
    private static final int NUM_2 = 2;
    private static final int NUM_3 = 3;
    private static final int NUM_10 = 10;
    private static final int NUM_8 = 8;
    private static final String GRANTED = "allPermissionsGranted [";
    private static final int NUM_200 = 200;
    private static final String kh = "]";
    private SuperButton fragmentAskOnePermission;
    private SuperButton fragmentAskThreePermission;
    private SuperButton askOnePermissionSimple;
    private SuperButton fragmentAskThreePermissionSimple;
    private SuperButton fragmentAskOnePermissionSmart;
    private SuperButton fragmentAskThreePermissionSmart;
    private SuperButton fragmentCheckPermission;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_ability_fragment, container, false);
        initComponent(component);
        return component;
    }

    private void initComponent(Component component) {
        fragmentAskOnePermission = (SuperButton) component.
                findComponentById(ResourceTable.Id_fragment_ask_one_permission);
        fragmentAskThreePermission = (SuperButton) component.
                findComponentById(ResourceTable.Id_fragment_ask_three_permission);
        askOnePermissionSimple = (SuperButton) component.
                findComponentById(ResourceTable.Id_fragment_ask_one_permission_simple);
        fragmentAskThreePermissionSimple = (SuperButton) component.
                findComponentById(ResourceTable.Id_fragment_ask_three_permission_simple);
        fragmentAskOnePermissionSmart = (SuperButton) component.
                findComponentById(ResourceTable.Id_fragment_ask_one_permission_smart);
        fragmentAskThreePermissionSmart = (SuperButton) component.
                findComponentById(ResourceTable.Id_fragment_ask_three_permission_smart);
        fragmentCheckPermission = (SuperButton) component.
                findComponentById(ResourceTable.Id_fragment_check_permission);

        fragmentAskOnePermission.setClickedListener(this);
        fragmentAskThreePermission.setClickedListener(this);
        askOnePermissionSimple.setClickedListener(this);
        fragmentAskThreePermissionSimple.setClickedListener(this);
        fragmentAskOnePermissionSmart.setClickedListener(this);
        fragmentAskThreePermissionSmart.setClickedListener(this);
        fragmentCheckPermission.setClickedListener(this);

        FragmentAbility fractionAbility = (FragmentAbility) getFractionAbility();
        fractionAbility.setRequestPermission(new RequestPermission() {
            @Override
            public void result(int requestCode, String[] permissions, int[] grantResults) {
                PermissionManager.handleResult(MainFraction.this, requestCode, permissions, grantResults);
            }
        });
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            /**
             * 申请一个权限
             */
            case ResourceTable.Id_fragment_ask_one_permission:
                askOnePermission2();
                break;
            case ResourceTable.Id_fragment_ask_three_permission:
                fragmentAskThreePermission2();
                break;
            case ResourceTable.Id_fragment_ask_one_permission_simple:
                fragmentAskOnePermissionSimple();
                break;
            case ResourceTable.Id_fragment_ask_three_permission_simple:
                fragmentAskThreePermissionSimple();
                break;
            case ResourceTable.Id_fragment_ask_one_permission_smart:
                fragmentAskOnePermissionSmart2();
                break;
            case ResourceTable.Id_fragment_ask_three_permission_smart:
                fragmentAskThreePermissionSmart2();
                break;
            case ResourceTable.Id_fragment_check_permission:
                checkPermission();
                break;
            default:
                break;
        }
    }

    private void checkPermission() {
        PermissionEnum permissionEnum = PermissionEnum.WRITE_EXTERNAL_STORAGE;
        boolean isGranted = PermissionUtils.isGranted(getFractionAbility(), PermissionEnum.WRITE_EXTERNAL_STORAGE);
        showToast(permissionEnum.toString() + " isGranted [" + isGranted + kh, getFractionAbility());
    }

    private void fragmentAskThreePermissionSmart2() {
        PermissionManager.Builder()
                .key(NUM_2101)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.ACCESS_FINE_LOCATION,
                        PermissionEnum.READ_CALENDAR)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean isAllPermissionsGranted, boolean isSomePermissionsDeniedForever) {
                        showToast(PermissionEnum.WRITE_EXTERNAL_STORAGE.toString()
                                + ", " + PermissionEnum.ACCESS_FINE_LOCATION.toString() + ", "
                                + PermissionEnum.READ_CALENDAR.toString()
                                + GRANTED + isAllPermissionsGranted + "] somePermissionsDeniedForever ["
                                + isSomePermissionsDeniedForever + kh, getFractionAbility());
                    }
                })
                .ask(MainFraction.this);
    }

    private void fragmentAskOnePermissionSmart2() {
        PermissionManager.Builder()
                .key(NUM_2001)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean isAllPermissionsGranted, boolean isSomePermissionsDeniedForever) {
                        showToast(PermissionEnum.WRITE_EXTERNAL_STORAGE.toString() + GRANTED
                                + isAllPermissionsGranted + "] somePermissionsDeniedForever ["
                                + isSomePermissionsDeniedForever + kh, getFractionAbility());
                    }
                })
                .ask(MainFraction.this);
    }

    private void fragmentAskThreePermissionSimple() {
        PermissionManager.Builder()
                .key(NUM_601)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.ACCESS_FINE_LOCATION,
                        PermissionEnum.READ_CALENDAR)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(new SimpleCallback() {
                    @Override
                    public void result(boolean isAllPermissionsGranted) {
                        showToast(PermissionEnum.WRITE_EXTERNAL_STORAGE.toString()
                                + ", " + PermissionEnum.ACCESS_FINE_LOCATION.toString() + ", "
                                + PermissionEnum.READ_CALENDAR.toString() + GRANTED
                                + isAllPermissionsGranted + kh, getFractionAbility());
                    }
                })
                .ask(MainFraction.this);
    }

    private void fragmentAskOnePermissionSimple() {
        PermissionManager.Builder()
                .key(NUM_701)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(new SimpleCallback() {
                    @Override
                    public void result(boolean isAllPermissionsGranted) {
                        showToast(PermissionEnum.WRITE_EXTERNAL_STORAGE.toString()
                                + GRANTED + isAllPermissionsGranted + kh, getFractionAbility());
                    }
                })
                .ask(MainFraction.this);
    }

    private void fragmentAskThreePermission2() {
        PermissionManager.Builder()
                .key(NUM_801)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.ACCESS_FINE_LOCATION,
                        PermissionEnum.READ_CALENDAR)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(MainFraction.this)
                .ask(MainFraction.this);
    }

    private void askOnePermission2() {
        PermissionManager.Builder()
                .key(NUM_9001)
                .permission(PermissionEnum.WRITE_EXTERNAL_STORAGE)
                .askAgain(true)
                .askAgainCallback(new AskAgainCallback() {
                    @Override
                    public void showRequestPermission(UserResponse response) {
                        showDialog(response);
                    }
                })
                .callback(MainFraction.this)
                .ask(MainFraction.this);
    }

    private void showDialog(AskAgainCallback.UserResponse response) {
        CommonDialog commonDialog = new CommonDialog(getFractionAbility());
        Component component1 = LayoutScatter.getInstance(getFractionAbility())
                .parse(ResourceTable.Layout_layout_dialog, null, true);
        inintDialogCommonDialog(component1, commonDialog, response);
        commonDialog.setContentCustomComponent(component1);
        Display display = DisplayManager.getInstance().getDefaultDisplay(getFractionAbility()).get();
        int width = display.getAttributes().width;
        int height = display.getAttributes().height;
        float height2 = (float) ((height / NUM_10) * NUM_3);
        commonDialog.setSize(width / NUM_10 * NUM_8, (int) height2);
        commonDialog.setMovable(false);
        commonDialog.show();
    }

    private void inintDialogCommonDialog(Component component,
                                         CommonDialog commonDialog, AskAgainCallback.UserResponse response) {
        component.findComponentById(ResourceTable.Id_no).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                response.result(false);
                if (commonDialog != null) {
                    commonDialog.hide();
                    commonDialog.remove();
                }
            }
        });
        component.findComponentById(ResourceTable.Id_ok).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                response.result(true);
                if (commonDialog != null) {
                    commonDialog.hide();
                    commonDialog.remove();
                }
            }
        });
    }

    /**
     * 显示Toast
     *
     * @param msg
     * @param ability
     */
    private void showToast(String msg, Ability ability) {
        DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(ability)
                .parse(ResourceTable.Layout_layout_toast_and_image, null, false);
        Text text = (Text) layout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText(msg);
        Display display = DisplayManager.getInstance().getDefaultDisplay(getFractionAbility()).get();
        int height = display.getAttributes().height;
        int width = display.getAttributes().width;
        new ToastDialog(ability)
                .setComponent(layout)
                .setSize(width / NUM_10 * NUM_8, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.CENTER)
                .setOffset(0, height / NUM_2 - NUM_200)
                .show();
    }

    @Override
    public void result(ArrayList<PermissionEnum> permissionsGranted,
                       ArrayList<PermissionEnum> permissionsDenied, ArrayList<PermissionEnum> permissionsDeniedForever,
                       ArrayList<PermissionEnum> permissionsAsked) {
        List<String> msg = new ArrayList<>();
        for (PermissionEnum permissionEnum : permissionsGranted) {
            msg.add(permissionEnum.toString() + " [Granted]");
        }
        for (PermissionEnum permissionEnum : permissionsDenied) {
            msg.add(permissionEnum.toString() + " [Denied]");
        }
        for (PermissionEnum permissionEnum : permissionsDeniedForever) {
            msg.add(permissionEnum.toString() + " [DeniedForever]");
        }
        for (PermissionEnum permissionEnum : permissionsAsked) {
            msg.add(permissionEnum.toString() + " [Asked]");
        }
        ListDialog listDialog = new ListDialog(getFractionAbility(), msg);
        listDialog.show();
    }
}
