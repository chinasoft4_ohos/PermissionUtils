/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.utils.net.Uri;

/**
 * 描述 infoDialog
 *
 * @since 2021-04-22
 */
public class InfoDialog extends CommonDialog {
    private static final int NUM_10 = 10;
    private static final int NUM_9 = 9;
    private static final int NUM_3 = 3;
    private Ability context;

    /**
     * 向外暴露的构造
     *
     * @param context 上下文
     * @param info 显示的信息
     */
    public InfoDialog(Ability context, String info) {
        super(context);
        this.context = context;
        initViewDialog();
    }

    private void initViewDialog() {
        Component component = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_info_dialog, null, true);
        initComponent(component);
        InfoDialog.this.setMovable(false);
        InfoDialog.this.setAlignment(LayoutAlignment.CENTER);
        InfoDialog.this.setTransparent(true);
        InfoDialog.this.setContentCustomComponent(component);
    }

    private void initComponent(Component component) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        int width = display.getAttributes().width;
        int height = display.getAttributes().height;
        int dialogWidth = (int) ((width / NUM_10) * NUM_9);
        int titleHeight = height / NUM_10;
        int dialogHeight = titleHeight * NUM_3;
        InfoDialog.this.setSize(dialogWidth, dialogHeight);
        final Text close = (Text) component.findComponentById(ResourceTable.Id_close);
        final Text github = (Text) component.findComponentById(ResourceTable.Id_github);
        final Text twitter = (Text) component.findComponentById(ResourceTable.Id_twitter);
        final Text goo = (Text) component.findComponentById(ResourceTable.Id_goo);
        final Text linkedin = (Text) component.findComponentById(ResourceTable.Id_linkedin);
        github.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                goGitHub();
            }
        });
        twitter.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                goTwitter();
            }
        });
        goo.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                goGoo();
            }
        });
        linkedin.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                goLinkedin();
            }
        });

        close.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                InfoDialog.this.destroy();
            }
        });
    }

    private void goLinkedin() {
        String link = context.getString(ResourceTable.String_Linkedin);
        Intent intent2 = new Intent();
        Operation operationCommonComponts = new Intent.OperationBuilder()
                .withUri(Uri.parse(link))
                .build();
        intent2.setOperation(operationCommonComponts);
        context.startAbility(intent2);
    }

    private void goGoo() {
        String goo = context.getString(ResourceTable.String_Goo);
        Intent intent2 = new Intent();
        Operation operationCommonComponts = new Intent.OperationBuilder()
                .withUri(Uri.parse(goo))
                .build();
        intent2.setOperation(operationCommonComponts);
        context.startAbility(intent2);
    }

    private void goTwitter() {
        String github = context.getString(ResourceTable.String_GitHub);
        Intent intent2 = new Intent();
        Operation operationCommonComponts = new Intent.OperationBuilder()
                .withUri(Uri.parse(github))
                .build();
        intent2.setOperation(operationCommonComponts);
        context.startAbility(intent2);
    }

    private void goGitHub() {
        String github = context.getString(ResourceTable.String_GitHub);
        Intent intent2 = new Intent();
        Operation operationCommonComponts = new Intent.OperationBuilder()
                .withUri(Uri.parse(github))
                .build();
        intent2.setOperation(operationCommonComponts);
        context.startAbility(intent2);
    }
}
