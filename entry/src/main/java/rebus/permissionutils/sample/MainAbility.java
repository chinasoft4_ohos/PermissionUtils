/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.sample.slice.MainAbilitySlice;

/**
 * 主页面
 *
 * @since 2021-04-12
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        /**
         * 沉浸式状态栏
         */
        getWindow().setStatusBarColor(Color.getIntColor("#FF4949FF"));
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionManager.handleResult(this, requestCode, permissions, grantResults);
    }
}
