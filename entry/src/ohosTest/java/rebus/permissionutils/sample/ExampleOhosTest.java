/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rebus.permissionutils.sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * 测试
 *
 * @since 2021-05-27
 */
public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        String abilityName = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().getAbilityName();
        assertEquals("rebus.permissionutils.sample", actualBundleName);
        System.out.println("测试通过！");
    }

    @Test
    public void isGranted(){
        boolean granted = PermissionUtils.isGranted(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility(), PermissionEnum.WRITE_EXTERNAL_STORAGE);
        assertFalse(granted);
    }
}